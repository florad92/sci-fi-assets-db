let app = new Vue({
  el: "#sci-fi-asset-db",
  data: {
    selectedTags: [],
    db: null,
  },
  computed: {
    tags: function () {
      let tags = [];
      this.db.forEach((e) => {
        e.tags.forEach((t) => {
          if (!tags.includes(t)) {
            tags.push(t);
          }
        });
      });
      return tags;
    },
    filteredArtists: function () {
      let comparator = (element) => this.selectedTags.includes(element);
      let retVal;
      if (this.selectedTags.length === 0) {
        retVal = this.db;
      } else {
        retVal = this.db.filter((a) => {
          return a.tags.some(comparator);
        });
      }
      return retVal.sort(this.nameComparator);
    },
  },
  methods: {
    selectTag: function (tag) {
      if (this.isSelected(tag)) {
        let index = this.selectedTags.indexOf(tag);
        if (index > -1) {
          this.selectedTags.splice(index, 1);
        }
      } else {
        this.selectedTags.push(tag);
      }
    },
    nameComparator: function (a, b) {
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    },
    isSelected: function (tag) {
      return this.selectedTags.includes(tag);
    },
  },
  created: function () {
    fetch("https://florad92.gitlab.io/sci-fi-assets-db/data.json")
      .then((stream) => stream.json())
      .then((data) => (this.db = data));
  },
});
